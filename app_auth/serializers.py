from django.contrib.auth import get_user_model
from rest_framework import serializers
from django.http.response import JsonResponse

User = get_user_model()


class RegisterUserSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        user_kwargs = {
            field_name: field_value
            for field_name, field_value in validated_data.items()
            if field_name != 'password'
        }

        user = User.objects.create(**user_kwargs)

        user.set_password(validated_data.get('password'))
        user.save()

        return user

    class Meta:
        model = User
        fields = (
            'username',
            'password',
            'first_name',
            'last_name'
        )
        extra_kwargs = {
            'password': {
                'write_only': True
            }
        }

class UpdateUserPasswordSerializer(serializers.Serializer):
    previous_password = serializers.CharField(max_length=32, required=True, write_only=True)
    new_password = serializers.CharField(max_length=32, required=True, write_only=True)

    def validate_previous_password(self, previous_password):
        request = self.context.get('request')

        if not request.user.check_password(previous_password):
            raise serializers.ValidationError('Previous password is not correct!')

        return previous_password

