from django.urls import path
from django.conf.urls import include

from app_auth.views import RegisterAPIView, ObtainTokenAPIView, ChangePasswordAPIView

urlpatterns = [
    path('register', RegisterAPIView.as_view(), name='register'),
    path('token-auth', ObtainTokenAPIView.as_view(), name='login'),
    path('password', ChangePasswordAPIView.as_view(), name='change-password')
]
