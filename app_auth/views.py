from rest_framework.viewsets import generics
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response

from app_auth.serializers import RegisterUserSerializer, UpdateUserPasswordSerializer
from django.contrib.auth import get_user_model
from rest_framework import authentication, permissions
from django.http import JsonResponse

import logging

User = get_user_model()

logger = logging.getLogger('base')


class RegisterAPIView(generics.CreateAPIView):
    serializer_class = RegisterUserSerializer


class ObtainTokenAPIView(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(
            data=request.data,
            context={'request': request}
        )
        serializer.is_valid(raise_exception=True)

        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)

        return Response({
            'token': token.key,
            'user_id': user.pk
        })


class ChangePasswordAPIView(generics.UpdateAPIView):
    serializer_class = UpdateUserPasswordSerializer
    queryset = User.objects.all()
    authentication_classes = [authentication.TokenAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get_object(self):
        return self.request.user

    def perform_update(self, serializer):
        user = self.get_object()

        new_password = serializer.validated_data.get('new_password')
        
        user.set_password(new_password)
        user.save()

        return user

    def update(self, request, *args, **kwargs):
        super().update(request, *args, **kwargs)

        return JsonResponse({
            'success': True
        })
