FROM python:3.8-alpine

WORKDIR /usr/src/app

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev gdal-dev

COPY requirements.txt .
RUN pip install -r requirements.txt

CMD python3 manage.py runserver 0.0.0.0:8000

EXPOSE 8000
