from django.db import models
from django.contrib.gis.db import models as gis_models


class Localisation(gis_models.Model):
    point = gis_models.PointField()
    start = models.DateTimeField()
    stop = models.DateTimeField()
