from datetime import datetime

from django.contrib.gis.geos import Point
from rest_framework import serializers

from localisation.models import Localisation


class LocalisationSerializer(serializers.Serializer):
    pk = serializers.IntegerField(read_only=True)
    start = serializers.DateTimeField()
    stop = serializers.DateTimeField()
    lat = serializers.FloatField()
    lon = serializers.FloatField()

    def to_representation(self, instance):
        representation = {
            "pk": instance.pk,
            "start": int(datetime.timestamp(instance.start)),
            "stop": int(datetime.timestamp(instance.stop)),
            "lat": instance.point.x,
            "lon": instance.point.y
        }

        return representation

    def create(self, validated_data):
        point = Point(validated_data["lat"], validated_data["lon"])
        return Localisation.objects.create(point=point, start=validated_data["start"], stop=validated_data["stop"])

    def update(self, instance, validated_data):
        instance.point = Point(validated_data["lat"], validated_data["lon"])
        instance.start = validated_data["start"]
        instance.stop = validated_data["stop"]
        return instance


class RectangleSerializer(serializers.Serializer):
    x = serializers.FloatField()
    y = serializers.FloatField()
    w = serializers.FloatField()
    h = serializers.FloatField()

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass
