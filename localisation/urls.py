from django.urls import path

from localisation import views


localisation_detail = views.LocalisationsViewSet.as_view({
    'get': 'retrieve',
    'post': 'create',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

localisation_list = views.LocalisationsViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', localisation_list),
    path('<int:pk>', localisation_detail),
    path('in-rectangle/', views.LocalisationsInRectangle.as_view())
]