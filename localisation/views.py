from django.contrib.gis.geos import Polygon
from rest_framework import viewsets, generics
from rest_framework.response import Response

from localisation.models import Localisation
from localisation.serializers import LocalisationSerializer, RectangleSerializer


class LocalisationsViewSet(viewsets.ModelViewSet):
    queryset = Localisation.objects.all()
    serializer_class = LocalisationSerializer


class LocalisationsInRectangle(generics.ListAPIView):
    serializer_class = RectangleSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        data = serializer.data

        try:
            x, y, w, h = data["x"], data["y"], data["w"], data["h"]
        except KeyError:
            return Response({"error": "incomplete rectangle coordinates"}, status=400)

        polygon = Polygon(((x, y), (x, y + h), (x + w, y + h), (x + w, y), (x, y)))

        queryset = Localisation.objects.filter(point__intersects=polygon)
        return Response(LocalisationSerializer(queryset, many=True).data)
